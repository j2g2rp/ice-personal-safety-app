#include <QDebug>
#include <QFile>
#include <QDir>
#include <QJsonDocument>
#include <QJsonObject>
#include <QFileInfo>

#include "settings.h"

Settings::Settings() {
    QFile config(m_configPath + "config.json");
    config.open(QFile::ReadOnly);

    QJsonDocument doc = QJsonDocument::fromJson(config.readAll());
    QJsonObject object = doc.object();

    m_doctornum = object.value("doctornum").toString().trimmed();
    m_doctorname = object.value("doctorname").toString().trimmed();
    m_infopersonnal = object.value("infopersonnal").toString().trimmed();

 
    Q_EMIT doctornumChanged(m_doctornum); 
    Q_EMIT doctornameChanged(m_doctorname);
    Q_EMIT infopersonnalChanged(m_infopersonnal);

    config.close();
}

void Settings::save() {
    QJsonObject object;
    object.insert("doctornum", QJsonValue(m_doctornum.trimmed()));
    object.insert("doctorname", QJsonValue(m_doctorname.trimmed()));
    object.insert("infopersonnal", QJsonValue(m_infopersonnal.trimmed()));
    
    QJsonDocument doc;
    doc.setObject(object);

    if (!QDir(m_configPath).exists()) {
        QDir().mkdir(m_configPath);
    }

    QFile config(m_configPath + "config.json");
    bool success = config.open(QFile::WriteOnly | QFile::Text | QFile::Truncate);
    config.write(doc.toJson());
    config.close();

    Q_EMIT saved(success);
}
