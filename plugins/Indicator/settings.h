#ifndef SETTINGS_H
#define SETTINGS_H

#include <QObject>

class Settings: public QObject {
    Q_OBJECT

    Q_PROPERTY(QString doctornum MEMBER m_doctornum NOTIFY doctornumChanged)
    Q_PROPERTY(QString doctorname MEMBER m_doctorname NOTIFY doctornameChanged)
    Q_PROPERTY(QString infopersonnal MEMBER m_infopersonnal NOTIFY infopersonnalChanged)

public:
    Settings();
    ~Settings() = default;

    Q_INVOKABLE void save();

Q_SIGNALS:
    void saved(bool success);

    void doctornumChanged(const QString &doctornum);
    void doctornameChanged(const QString &doctorname);
    void infopersonnalChanged(const QString &infopersonnal);

private:
    QString m_configPath = "/home/phablet/.config/indicator-ice.ubuntouchfr/"; //TODO don't hardcode this

    QString m_doctornum;
    QString m_doctorname;
    QString m_infopersonnal;
};

#endif
